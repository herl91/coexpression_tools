#!/usr/bin/env Rscript
args=commandArgs(trailingOnly=TRUE)

helptext <-"usage:
makeEdgeList.R expression_file edges_file [options]

options:
    -m / --method               pearson (default), spearman
    -c / --cutoff               Minimum correlation coefficient threshold (0.8 default)
    -a / --attributes_file      For cytoscape usage.
    -f / --filter_file          Only use the nodes in this list.
    -m / --matrix_output_file   Full edge matrix output.
    -k / --no_cutoff            Full output
    -d / --mad_filter           Removes genes with a MAD of 0 from calculations.
    -f / --full                 Use for attribute output.
    -v / --verbose\n"

cor_method<-'pearson'
cutoff<-0.8
attributes_file<-FALSE
filter_file<-FALSE
matrix_output_file<-FALSE
full<-FALSE
use_cutoff<-TRUE
mad_filter<-FALSE
verbose<-FALSE

################### INPUT READING
if(length(args)<2){
    cat(helptext)
    stop('At least two argument must be supplied.')
} else{
    inFile<-args[1]
    outFile<-args[2]
    if(length(args)>2){
        for(n in 3:length(args)){
            if(args[n]=='-m'||args[n]=='--method'){cor_method<-args[n+1]}
            if(args[n]=='-c'||args[n]=='--cutoff'){cutoff<-as.numeric(args[n+1])}
            if(args[n]=='-a'||args[n]=='--attributes_file'){attributes_file<-args[n+1]}
            if(args[n]=='-f'||args[n]=='--filter_file'){filter_file<-args[n+1]}
            if(args[n]=='-m'||args[n]=='--matrix_output_file'){matrix_output_file<-args[n+1]}
            if(args[n]=='-k'||args[n]=='--no_cutoff'){use_cutoff<-FALSE}
            if(args[n]=='-d'||args[n]=='--mad_filter'){mad_filter<-TRUE}
            if(args[n]=='-f'||args[n]=='--full'){full<-TRUE}
            if(args[n]=='-v'||args[n]=='--verbose'){verbose<-TRUE}
        }
    }
}
###################


################### LOAD OPTIONALS
if(attributes_file!=FALSE){
    if(verbose==TRUE){print('Loading attributes file...')}
    raw.data <- read.table(attributes_file,header=F,stringsAsFactors=F)
    attributes <-data.frame(raw.data$V2,stringsAsFactors=F)
    tmp <- character()
    for(i in raw.data$V1){
        newname <- substr(i,1,16)
        tmp <- c(tmp,newname)
    }
    rownames(attributes) <- tmp
    colnames(attributes) <- 'attribute'
    if(verbose==TRUE){print('Done!')}
}

if(filter_file!=FALSE){
    if(verbose==TRUE){print('Loading filter file...')}
    nodes <- scan(filter_file,sep='\n',what='')
    if(verbose==TRUE){print('Done!')}
}
###################


################### MAIN
if(verbose==TRUE){print('Loading input...')}
raw.data <- read.csv(file=inFile, header=TRUE, comment.char='#')
acc_snp <- raw.data[,2:ncol(raw.data)]
rownames(acc_snp) <- raw.data[,1]
remove(raw.data)

if(filter_file!=FALSE){
    if(verbose==TRUE){print('Filtering by nodes...')}
    matches <- !is.na(match(rownames(acc_snp),nodes))
    acc_snp<-acc_snp[matches,]
}

if(mad_filter==TRUE){
    if(verbose==TRUE){print('Applying MAD...')}
    mads <- apply(acc_snp,1,mad)
    acc_snp$MAD <- mads
    acc_snp <- acc_snp[acc_snp$MAD>0,]
    acc_snp$MAD <- NULL
}

if(verbose==TRUE){print('Correlating...')}
acc_snp_t<-t(acc_snp)
remove(acc_snp)
acc_snp.cor = cor(acc_snp_t,method = cor_method)
remove(acc_snp_t)

if(matrix_output_file!=FALSE){
    if(verbose==TRUE){print('Writing matrix output...')}
    write.csv(acc_snp.cor, file=matrix_output_file, row.names = T,quote=FALSE)
}


if(verbose==TRUE){print('Organizing...')}
down <- lower.tri(acc_snp.cor,diag=FALSE)
out <- data.frame(which(down,arr.ind=TRUE),score=acc_snp.cor[down])
remove(down)
colnames(out) <- c('source','target','score')
out$source <- rownames(acc_snp.cor)[out$source]
out$target <- rownames(acc_snp.cor)[out$target]

if(use_cutoff==TRUE){
    edgeFrame <- out[out$score>cutoff,]
}else{
    edgeFrame <- out
}
remove(out)


if(attributes_file!=FALSE){
    if(verbose==TRUE){print('Adding attributes...')}
    edgeFrame$source_attribute <- attributes[edgeFrame$source,'attribute']
    edgeFrame$target_attribute <- attributes[edgeFrame$target,'attribute']
    edgeFrame$source_attribute[is.na(edgeFrame$source_attribute)]='NA'
    edgeFrame$target_attribute[is.na(edgeFrame$target_attribute)]='NA'
    edgeFrame$source_attribute<-gsub(',',';',edgeFrame$source_attribute)
    edgeFrame$target_attribute<-gsub(',',';',edgeFrame$target_attribute)

    x<-matrix(c(edgeFrame$source_attribute,edgeFrame$target_attribute),nrow=2,byrow=TRUE)
    y<-apply(x,2,sort)
    z<-paste(y[1,],y[2,],sep=' <-> ')
    edgeFrame$interaction <- z

    if(full==FALSE){
        edgeFrame$source_attribute<-NULL
        edgeFrame$target_attribute<-NULL
    }
}

if(verbose==TRUE){print('Writing...')}
write.csv(edgeFrame, file=outFile, row.names = FALSE,quote=FALSE)

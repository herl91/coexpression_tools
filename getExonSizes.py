#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function
import os
import sys
import numpy as np


class Options(object):
    input_file = ''
    output_file = ''


def parse_input(argv):
    files_list = []

    for n, arg in enumerate(argv):
            files_list.append(argv[n])

    Options.input_file = files_list[0]
    Options.output_file = files_list[1]
    return


#################
# MAIN PIPELINE #
#################
def main(argv):
    parse_input(argv)
    gff = np.genfromtxt(Options.input_file,delimiter='\t',dtype='str')
    d={}

    for cur_line in gff:
        if cur_line[0].startswith('#'):
            continue
        else:
            ttype = cur_line[2]
            start = int(cur_line[3])
            end = int(cur_line[4])
            size = end-start+1
            if ttype=='exon':
                gene = cur_line[8].split(';')[1].split(':')[1][:-2]
                if gene not in d:
                    d[gene]=0
                d[gene] += size

    dlist = [[k,d[k]] for k in d]
    np.savetxt(Options.output_file,dlist,fmt='%s',delimiter='\t')
    return


if __name__ == "__main__":
    main(sys.argv[1:])